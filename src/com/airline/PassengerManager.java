package com.airline;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PassengerManager {
    public static void updatePassengerInformation(Connection connection, int passengerId, String newName, int newAge, String newGender, String newPhoneNumber) {
        try {
            // Prepare SQL statement to update passenger information
            String sql = "UPDATE Passenger SET name = ?, age = ?, gender = ?, phone_number = ? WHERE passenger_id = ?";
            PreparedStatement statement = connection.prepareStatement(sql);

            // Set parameters
            statement.setString(1, newName);
            statement.setInt(2, newAge);
            statement.setString(3, newGender);
            statement.setString(4, newPhoneNumber);
            statement.setInt(5, passengerId);

            // Execute the update statement
            int rowsUpdated = statement.executeUpdate();
            if (rowsUpdated > 0) {
                System.out.println("Passenger information updated successfully.");
            } else {
                System.out.println("Failed to update passenger information. Passenger ID not found.");
            }

            // Close resources
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
