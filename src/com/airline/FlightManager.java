
package com.airline;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FlightManager {
    public static void insertFlightDetails(Connection connection, String flightNumber, String source, String destination,
                                           java.sql.Date departureDate, java.sql.Time departureTime, int capacity) {
        try {
            // Prepare SQL statement
            String sql = "INSERT INTO Flight (flight_number, source, destination, departure_date, departure_time, capacity) VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(sql);

            // Set parameters
            statement.setString(1, flightNumber);
            statement.setString(2, source);
            statement.setString(3, destination);
            statement.setDate(4, departureDate);
            statement.setTime(5, departureTime);
            statement.setInt(6, capacity);

            // Execute the INSERT statement
            int rowsInserted = statement.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("Flight details inserted successfully.");
            } else {
                System.out.println("Failed to insert flight details.");
            }

            // Close resources
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public static void displayVacancySeatDetails(Connection connection, String source, String destination) {
        try {
            // Prepare SQL statement to retrieve vacancy seat details based on source and destination
            String sql = "SELECT * FROM Flight WHERE source = ? AND destination = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, source);
            statement.setString(2, destination);
            
            // Execute the query
            ResultSet resultSet = statement.executeQuery();
            
            // Display vacancy seat details
            while (resultSet.next()) {
                int flightId = resultSet.getInt("flight_id");
                String flightNumber = resultSet.getString("flight_number");
                int capacity = resultSet.getInt("capacity");
                int bookedSeats = getBookedSeats(connection, flightId);
                int vacancy = capacity - bookedSeats;
                System.out.println("Flight ID: " + flightId);
                System.out.println("Flight Number: " + flightNumber);
                System.out.println("Vacancy Seats: " + vacancy);
            }
            
            // Close resources
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    private static int getBookedSeats(Connection connection, int flightId) throws SQLException {
        // Prepare SQL statement to retrieve booked seats for a flight
        String sql = "SELECT COUNT(*) AS booked_seats FROM Ticket WHERE flight_id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, flightId);
        
        // Execute the query
        ResultSet resultSet = statement.executeQuery();
        
        // Get the count of booked seats
        int bookedSeats = 0;
        if (resultSet.next()) {
            bookedSeats = resultSet.getInt("booked_seats");
        }
        
        // Close resources
        resultSet.close();
        statement.close();
        
        return bookedSeats;
    }
    public static void updateFlightDetails(Connection connection, int flightId, String newFlightNumber, String newSource, String newDestination, String newDepartureDate, String newDepartureTime, int newCapacity) {
        try {
            // Prepare SQL statement to update flight details
            String sql = "UPDATE Flight SET flight_number = ?, source = ?, destination = ?, departure_date = ?, departure_time = ?, capacity = ? WHERE flight_id = ?";
            PreparedStatement statement = connection.prepareStatement(sql);

            // Set parameters
            statement.setString(1, newFlightNumber);
            statement.setString(2, newSource);
            statement.setString(3, newDestination);
            statement.setString(4, newDepartureDate);
            statement.setString(5, newDepartureTime);
            statement.setInt(6, newCapacity);
            statement.setInt(7, flightId);

            // Execute the update statement
            int rowsUpdated = statement.executeUpdate();
            if (rowsUpdated > 0) {
                System.out.println("Flight details updated successfully.");
            } else {
                System.out.println("Failed to update flight details. Flight ID not found.");
            }

            // Close resources
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void updateBookedSeats(Connection connection, int flightId, int seatsChange) {
        try {
            // Query to update the booked seats for the flight
            String updateQuery = "UPDATE Flight SET booked_seats = booked_seats + ? WHERE flight_id = ?";
            PreparedStatement updateStatement = connection.prepareStatement(updateQuery);
            
            // Set parameters
            updateStatement.setInt(1, seatsChange);
            updateStatement.setInt(2, flightId);

            // Execute the update statement
            int rowsUpdated = updateStatement.executeUpdate();

            if (rowsUpdated > 0) {
                System.out.println("Booked seats for flight ID " + flightId + " updated successfully.");
            } else {
                System.out.println("Failed to update booked seats for flight ID " + flightId + ".");
            }

            // Close resources
            updateStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}


