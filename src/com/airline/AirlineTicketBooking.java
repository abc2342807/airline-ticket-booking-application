package com.airline;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class AirlineTicketBooking {
    private static final Scanner scanner = new Scanner(System.in);
    private static DatabaseManager dbManager;
    

    public static void main(String[] args) {
       
        try {
        	dbManager = new DatabaseManager();
            displayMenu();
            dbManager.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void displayMenu() {
        int choice;
        do {
        	
        	 System.out.println();
            System.out.println("Airline Ticket Booking System");
            System.out.println("1. Insert Passenger Details");
            System.out.println("2. Insert Flight Details");
            System.out.println("3. Book Ticket");
            System.out.println("4. Cancel Ticket");
            System.out.println("5. Display Vacancy Seat Details");
            System.out.println("6. Update Passenger Information");
            System.out.println("7. Update Flight Details");
            System.out.println("0. Exit");
            System.out.print("Enter your choice: ");
            choice = scanner.nextInt();
            scanner.nextLine(); // Consume newline
            switch (choice) {
                case 1:
                    insertPassengerDetails();
                    break;
                case 2:
                    insertFlightDetails();
                    break;
                case 3:
                    bookTicket();
                    break;
                case 4:
                    cancelTicket();
                    break;
                case 5:
                    displayVacancySeatDetails();
                    break;
                case 6:
                    updatePassengerInformation();
                    break;
                case 7:
                    updateFlightDetails();
                    break;
                case 0:
                    System.out.println("Exiting...");
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
                    break;
            }
        } while (choice != 0);
    }

    private static void insertPassengerDetails() {
    	try {
            System.out.println("Inserting Passenger Details...");
            System.out.print("Enter passenger name: ");
            String name = scanner.nextLine();
            System.out.print("Enter passenger age: ");
            int age = scanner.nextInt();
            scanner.nextLine(); // Consume newline
            System.out.print("Enter passenger gender: ");
            String gender = scanner.nextLine();
            System.out.print("Enter passenger phone number: ");
            String phoneNumber = scanner.nextLine();

            // Establish connection
            Connection connection = dbManager.getConnection();

            // Prepare SQL statement
            String sql = "INSERT INTO Passenger (name, age, gender, phone_number) VALUES (?, ?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(sql);

            // Set parameters
            statement.setString(1, name);
            statement.setInt(2, age);
            statement.setString(3, gender);
            statement.setString(4, phoneNumber);

            // Execute the INSERT statement
            int rowsInserted = statement.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("Passenger details inserted successfully.");
            } else {
                System.out.println("Failed to insert passenger details.");
            }

            // Close resources
            statement.close();
            connection.close();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
//    insertFlightDetails 
    
    private static void insertFlightDetails() {
    	System.out.println("Inserting Flight Details...");
        System.out.print("Enter flight number: ");
        String flightNumber = scanner.nextLine();
        System.out.print("Enter source: ");
        String source = scanner.nextLine();
        System.out.print("Enter destination: ");
        String destination = scanner.nextLine();
        System.out.print("Enter departure date (YYYY-MM-DD): ");
        String departureDateStr = scanner.nextLine();
        System.out.print("Enter departure time (HH:MM:SS): ");
        String departureTimeStr = scanner.nextLine();
        System.out.print("Enter capacity: ");
        int capacity = scanner.nextInt();
        scanner.nextLine(); // Consume newline

        // Parse departure date and time
        java.sql.Date departureDate = java.sql.Date.valueOf(departureDateStr);
        java.sql.Time departureTime = java.sql.Time.valueOf(departureTimeStr);

        // Get database connection from DatabaseManager
        Connection connection = dbManager.getConnection();

        // Call insertFlightDetails() method from FlightManager
        FlightManager.insertFlightDetails(connection, flightNumber, source, destination, departureDate, departureTime, capacity);

        // Close the database connection
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//    bookTicket
    
    private static void bookTicket() {
        try {
            
            // Get passengerId and flightId from user input or elsewhere
            System.out.print("Enter passenger ID: ");
            int passengerId = scanner.nextInt();
            scanner.nextLine(); // Consume newline
            System.out.print("Enter flight ID: ");
            int flightId = scanner.nextInt();
            scanner.nextLine(); // Consume newline
            
            // Get database connection from DatabaseManager
            try (Connection connection = dbManager.getConnection()) {
                TicketManager.bookTicket(connection, passengerId, flightId);
            }System.out.println("Booking Ticket...");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    
//    cancelTicket
    
    private static void cancelTicket() {
        // Add logic to get ticketId and flightId from user input
        System.out.println("Enter ticketId : ");
        int ticketId = scanner.nextInt();
        System.out.println("Enter flightId : ");
        int flightId = scanner.nextInt();

        // Get database connection from DatabaseManager
        Connection connection = dbManager.getConnection();

        // Call cancelTicket() method from TicketManager
        TicketManager.cancelTicket(connection, ticketId, flightId);
        System.out.println("Cancelling Ticket...");

        // Close the database connection
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


//    displayVacancySeatDetails
    private static void displayVacancySeatDetails() {
        System.out.println("Displaying Vacancy Seat Details...");
        System.out.print("Enter source: ");
        String source = scanner.nextLine();
        System.out.print("Enter destination: ");
        String destination = scanner.nextLine();
        
        // Get database connection from DatabaseManager
        try (Connection connection = dbManager.getConnection()) {
            // Call displayVacancySeatDetails() method from FlightManager
            FlightManager.displayVacancySeatDetails(connection, source, destination);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



//    updatePassengerInformation
    
    private static void updatePassengerInformation() {
       
        int passengerId = getPassengerIdFromUserInput();
        String newName = getNewNameFromUserInput();
        int newAge = getNewAgeFromUserInput();
        String newGender = getNewGenderFromUserInput();
        String newPhoneNumber = getNewPhoneNumberFromUserInput();

        // Get database connection from DatabaseManager
        Connection connection = dbManager.getConnection();

        // Call updatePassengerInformation() method from PassengerManager
        PassengerManager.updatePassengerInformation(connection, passengerId, newName, newAge, newGender, newPhoneNumber);
        System.out.println("Updating Passenger Information...");
        // Close the database connection
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
//    getPassengerIdFromUserInput
    
    private static int getPassengerIdFromUserInput() {
        System.out.print("Enter Passenger ID: ");
        return scanner.nextInt();
    }

    private static String getNewNameFromUserInput() {
        scanner.nextLine(); // Consume newline
        System.out.print("Enter New Name: ");
        return scanner.nextLine();
    }

    private static int getNewAgeFromUserInput() {
    	
        System.out.print("Enter New Age: ");
        return scanner.nextInt();
    }

    private static String getNewGenderFromUserInput() {
        scanner.nextLine(); // Consume newline
        System.out.print("Enter New Gender: ");
        return scanner.nextLine();
    }

    private static String getNewPhoneNumberFromUserInput() {
        scanner.nextLine(); // Consume newline
        System.out.print("Enter New Phone Number: ");
        return scanner.nextLine();
    }
    
//    *******************************************


    private static void updateFlightDetails() {
        System.out.println("Updating Flight Details...");
        int flightId = getFlightIdFromUserInput();
        String newFlightNumber = getNewFlightNumberFromUserInput();
        String newSource = getNewSourceFromUserInput();
        String newDestination = getNewDestinationFromUserInput();
        String newDepartureDate = getNewDepartureDateFromUserInput();
        String newDepartureTime = getNewDepartureTimeFromUserInput();
        int newCapacity = getNewCapacityFromUserInput();

        // Get database connection from DatabaseManager
        Connection connection = dbManager.getConnection();

        // Call updateFlightDetails() method from FlightManager
        FlightManager.updateFlightDetails(connection, flightId, newFlightNumber, newSource, newDestination, newDepartureDate, newDepartureTime, newCapacity);

        // Close the database connection
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
 // Methods to get user input for updating flight details

    private static int getFlightIdFromUserInput() {
        System.out.print("Enter Flight ID: ");
        return scanner.nextInt();
    }

    private static String getNewFlightNumberFromUserInput() {
        scanner.nextLine(); // Consume newline
        System.out.print("Enter New Flight Number: ");
        return scanner.nextLine();
    }

    private static String getNewSourceFromUserInput() {
        System.out.print("Enter New Source: ");
        return scanner.nextLine();
    }

    private static String getNewDestinationFromUserInput() {
        System.out.print("Enter New Destination: ");
        return scanner.nextLine();
    }

    private static String getNewDepartureDateFromUserInput() {
        System.out.print("Enter New Departure Date (YYYY-MM-DD): ");
        return scanner.nextLine();
    }

    private static String getNewDepartureTimeFromUserInput() {
        System.out.print("Enter New Departure Time (HH:MM:SS): ");
        return scanner.nextLine();
    }

    private static int getNewCapacityFromUserInput() {
        System.out.print("Enter New Capacity: ");
        return scanner.nextInt();
    }
}
