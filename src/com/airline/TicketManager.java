package com.airline;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TicketManager {
    public static void bookTicket(Connection connection, int passengerId, int flightId) {
        try {
            // Prepare SQL statement
            String sql = "INSERT INTO Booking (passenger_id, flight_id) VALUES (?, ?)";
            PreparedStatement statement = connection.prepareStatement(sql);

            // Set parameters
            statement.setInt(1, passengerId);
            statement.setInt(2, flightId);

            // Execute the INSERT statement
            int rowsInserted = statement.executeUpdate();
            if (rowsInserted >= 0) {
                System.out.println("Ticket booked successfully.");
            } else {
                System.out.println("Failed to book ticket.");
            }
            FlightManager.updateBookedSeats(connection, flightId, 1); // Increment booked seats by 1


            // Close resources
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public static void cancelTicket(Connection connection, int ticketId, int flightId) {
        try {
            // Prepare SQL statement
            String sql = "DELETE FROM Ticket WHERE ticket_id = ?";
            PreparedStatement statement = connection.prepareStatement(sql);

            // Set parameters
            statement.setInt(1, ticketId);

            // Execute the DELETE statement
            int rowsDeleted = statement.executeUpdate();
            if (rowsDeleted >= 0) {
                System.out.println("Ticket canceled successfully.");
                // Update booked seats for the corresponding flight
                FlightManager.updateBookedSeats(connection, flightId, -1);
            } else {
                System.out.println("Failed to cancel ticket. Ticket ID not found.");
            }

            // Close resources
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    
    
    
    
}
